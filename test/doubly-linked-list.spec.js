'use strict'

const { expect } = require('chai')

const { DoublyLinkedList } = require('../src/doubly-linked-list')

describe('Doubly Linked Lists - Tests', () => {
  it('Constructor should intialize nodes values in correct order', () => {
    // Act
    const dbly = new DoublyLinkedList([3, 11, 2])

    // Assert
    expect(dbly.head).to.deep.include({ value: 3, prev: null })
    expect(dbly.head.next).to.deep.include({ value: 11 })
    expect(dbly.tail).to.deep.include({ value: 2, next: null })
  })

  it('Inserting to head should replace head and readjust first node references', () => {
    // Prepare
    const dbly = new DoublyLinkedList([2, 11, 1])

    // Act
    dbly.insertToHead(18)

    // Assert
    expect(dbly.head).to.deep.include({ value: 18, prev: null })
    expect(dbly.head.next).to.deep.include({ value: 2 })
  })

  it('Inserting to end should replace tail and readjust final node references', () => {
    // Prepare
    const dbly = new DoublyLinkedList([7, 12, 14])

    // Act
    dbly.insertToTail(18)

    // Assert
    expect(dbly.tail).to.deep.include({ value: 18, next: null })
    expect(dbly.tail.prev).to.deep.include({ value: 14 })
  })

  it('Inserting at any index should readjust the list references', () => {
    // Prepare
    const dbly = new DoublyLinkedList([7, 12, 14])

    // Act
    dbly.insertAt(1, 99)

    // Assert
    expect(dbly.head).to.deep.include({ value: 7, prev: null })
    expect(dbly.head.next).to.deep.include({ value: 99 })
    expect(dbly.head.next.next).to.deep.include({ value: 12 })
  })

  it('Inserting at first position index should readjust the list references', () => {
    // Prepare
    const dbly = new DoublyLinkedList([7, 12, 14])

    // Act
    dbly.insertAt(0, 99)

    // Assert
    expect(dbly.head).to.deep.include({ value: 99, prev: null })
    expect(dbly.head.next).to.deep.include({ value: 7 })
    expect(dbly.head.next.next).to.deep.include({ value: 12 })
    expect(dbly.tail).to.deep.include({ value: 14, next: null })
  })

  it('Inserting at out of bounds index should not change the list values', () => {
    // Prepare
    const dbly = new DoublyLinkedList([7, 12, 14])

    // Act
    dbly.insertAt(1000, 99)

    // Assert
    expect(dbly.head).to.deep.include({ value: 7, prev: null })
    expect(dbly.head.next).to.deep.include({ value: 12 })
    expect(dbly.tail).to.deep.include({ value: 14 })
  })

  it('Removing at first position index should readjust the list references', () => {
    // Prepare
    const dbly = new DoublyLinkedList([3, 4, 7])

    // Act
    dbly.removeAt(0)

    // Assert
    expect(dbly.head).to.deep.include({ value: 4, prev: null })
    expect(dbly.head.next).to.deep.include({ value: 7 })
    expect(dbly.tail).to.deep.include({ value: 7, next: null })
  })

  it('Removing at any index should readjust the list references', () => {
    // Prepare
    const dbly = new DoublyLinkedList([3, 4, 7])

    // Act
    dbly.removeAt(1)

    // Assert
    expect(dbly.head).to.deep.include({ value: 3, prev: null })
    expect(dbly.head.next).to.deep.include({ value: 7 })
    expect(dbly.tail).to.deep.include({ value: 7, next: null })
  })

  it('Removing at last position index should readjust the list references', () => {
    // Prepare
    const dbly = new DoublyLinkedList([3, 4, 7])

    // Act
    dbly.removeAt(2)

    // Assert
    expect(dbly.head).to.deep.include({ value: 3, prev: null })
    expect(dbly.tail).to.deep.include({ value: 4, next: null })
  })

  it('Removing at out of bounds index should not change list values', () => {
    // Prepare
    const dbly = new DoublyLinkedList([3, 4, 7])

    // Act
    dbly.removeAt(1000)

    // Assert
    expect(dbly.head).to.deep.include({ value: 3, prev: null })
    expect(dbly.head.next).to.deep.include({ value: 4 })
    expect(dbly.tail).to.deep.include({ value: 7 })
  })
})
