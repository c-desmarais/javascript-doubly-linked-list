class DoublyLinkedList {
  constructor (values) {
    const nodesArray = []

    // construct an array of nodes with no reference to each other
    for (let i = 0; i < values.length; i++) {
      const currentValue = values[i]
      const currentNode = {
        value: currentValue,
        prev: null,
        next: null
      }
      nodesArray.push(currentNode)
    }

    // add the references (previous and next) to each node
    for (let i = 0; i < nodesArray.length; i++) {
      nodesArray[i].next = nodesArray[i + 1] || null
      nodesArray[i].prev = nodesArray[i - 1] || null
    }

    this.head = nodesArray[0]
    this.tail = nodesArray[nodesArray.length - 1]
  }

  // insert a node at the top of the list
  insertToHead (value) {
    const insertedNode = { value, next: this.head, prev: null }
    this.head.prev = insertedNode
    this.head = insertedNode

    return insertedNode
  }

  // insert a node at the end of the list
  insertToTail (value) {
    const insertedNode = { value, next: null, prev: this.tail }
    this.tail.next = insertedNode
    this.tail = insertedNode

    return insertedNode
  }

  // insert a node at a given index position
  insertAt (index, value) {
    if (index === 0) return this.insertToHead(value)

    let currentNode = this.head

    let i = 0
    while (currentNode !== null) {
      if (i === index - 1) {
        const insertedNode = { value, next: null, prev: null }
        const nextNodeTmp = currentNode.next // could be null if appended to the end

        currentNode.next = insertedNode
        insertedNode.prev = currentNode
        insertedNode.next = nextNodeTmp

        if (nextNodeTmp) {
          nextNodeTmp.prev = insertedNode
        }

        return insertedNode.value
      } else {
        i++
        currentNode = currentNode.next
      }
    }

    console.warn('Log: Index is out of bounds')
    return null
  }

  // remove a node at a given index position
  removeAt (index) {
    let i = 0
    let currentNode = this.head

    while (currentNode !== null) {
      if (i === index) {
        // remove the node and replace the pointers
        if (currentNode.prev === null) {
          // remove the first node
          this.head = this.head.next
          this.head.prev = null
        } else if (currentNode.next === null) {
          // remove the tail node
          this.tail = this.tail.prev
          this.tail.next = null
        } else {
          currentNode.prev.next = currentNode.next
          currentNode.next.prev = currentNode.prev
        }

        return currentNode.value
      } else { // we did not find the index we are looking for
        currentNode = currentNode.next
        i++
      }
    }

    console.warn('Log: Index is out of bounds')

    return null
  }
}

module.exports = {
  DoublyLinkedList
}
