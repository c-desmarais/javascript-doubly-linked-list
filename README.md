# Javascript - Doubly Linked-List

[![pipeline status](https://gitlab.com/c-desmarais/javascript-doubly-linked-list/badges/master/pipeline.svg)](https://gitlab.com/c-desmarais/javascript-doubly-linked-list/-/commits/master)

[![coverage](https://gitlab.com/c-desmarais/javascript-doubly-linked-list/badges/master/coverage.svg)](https://gitlab.com/c-desmarais/javascript-doubly-linked-list/-/commits/master)

Here is an implementation of a Javascript doubly linked list.

## Advantages
- Nodes can be easily removed or added from a linked list without
reorganizing the entire data structure.

## Disadvantages
- Search operations are slow in linked lists.
- Random access is not allowed, we must start from the first node if we want to
access an element.
- It uses more memory than arrays because of the storage of the pointers.

## Usage
Here is an example of how to use it:

```javascript
const dbly = new DoublyLinkedList([1, 2, 3, 4, 6])
dbly.insertAt(2, 33)
```

## API

### new DoublyLinkedList(values)

Creates a new DoublyLinkedList. The values is an array of values (i.e. [2, 3, 4]).

### insertToHead(data)

Inserts a node at the begining of the linked list.

### insertToTail(data)

Inserts a node at the end of the linked list.

### insertAt(index, value)

Inserts a node with a given value at a specific place (index) in the linked list.

### removeAt(index)

Removes a node at a specific place (index) in the linked list.

## Development

Make sure to run : `npm install`

To lint the code, a simple `npm run lint` will do.

To run tests, simply run `npm run test` :hibiscus: